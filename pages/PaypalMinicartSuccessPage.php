<?php

class PaypalMinicartSuccessPage extends Page {
	function requireDefaultRecords() {
		parent::requireDefaultRecords();

		if(!DataObject::get_one('PaypalMinicartSuccessPage')) {
			$page = new PaypalMinicartSuccessPage();
			$page->Title = _t('PaypalMinicartSuccessPage.TITLE', 'Thank you very much for your purchase');
			$page->URLSegment = 'buy-online-success';
			$page->Content = _t('PaypalMinicartSuccessPage.CONTENT','<p>Thank you very much. You have successfully purchased and paid onto our PayPal account.</p>');
			$page->Status = 'Published';
			$page->ParentID = 0;
			$page->ShowInMenus = 0;
			$page->ShowInSearch = 0;
			$page->write();
			$page->publish('Stage', 'Live');
			DB::alteration_message('PaypalMinicartSuccessPage created','created');
		}
	}
}

class PaypalMinicartSuccessPage_Controller extends Page_Controller {
}

<?php

class MiniCartProductPage extends Page {

	private static $db = array(
		'ProductVariationSelectorLabel' => 'Varchar'
	);

	private static $defaults = array(
		'ProductVariationSelectorLabel' => 'Variation'
	);

	private static $has_many = array(
		'ProductVariations' => 'ProductVariation'
	);

	private static $many_many = array(
		'ProductImages' => 'Image'
	);

	private static $many_many_extraFields = array(
		'ProductImages' => array('SortOrder' => 'Int')
	);

	public function getCMSFields() {
		$fields = parent::getCMSFields();

		// variations
		$gridFieldConfig = GridFieldConfig::create()->addComponents(
			new GridFieldToolbarHeader(),
			new GridFieldAddNewButton('toolbar-header-right'),
			new GridFieldSortableHeader(),
			new GridFieldDataColumns(),
			new GridFieldPaginator(20),
			new GridFieldEditButton(),
			new GridFieldDeleteAction(),
			new GridFieldDetailForm()
		);
		if(class_exists('GridFieldSortableRows')) {
			$gridFieldConfig->addComponents(new GridFieldSortableRows('SortID'));
		} elseif(class_exists('GridFieldOrderableRows')) {
			$gridFieldConfig->addComponents(new GridFieldOrderableRows('SortID'));
		}
		$gridField = new GridField("ProductVariations", "ProductVariations", $this->owner->ProductVariations(), $gridFieldConfig);
		$fields->addFieldToTab("Root.ProductVariations", $gridField);
		if($this->owner->ProductVariations()->Count() > 1) {
			$fields->addFieldToTab("Root.ProductVariations", Textfield::create('ProductVariationSelectorLabel', 'Selector Label'));
		}

		// images
		$UploadFieldClass = class_exists('SortableUploadField') ? 'SortableUploadField' : 'UploadField';
		$fields->addFieldToTab('Root.ProductImages', $UploadFieldClass::create('ProductImages')
			->setFolderName('ProductImages')
			->setAllowedMaxFileNumber(10)
			->setAllowedExtensions(array('jpg', 'jpeg', 'png'))
		);

		return $fields;
	}

	/**
	* get the first variations image
	*
	*/
	public function Image() {
		foreach($this->ProductImages()->Sort('SortOrder') as $Image) {
			if($Image->exists()) {
				return $Image;
			}
		}
	}
}

class MiniCartProductPage_Controller extends Page_Controller {
	public function ProductImages() {
		return $this->data()->ProductImages()->sort(array('SortOrder' =>'ASC'));
	}

	public function MiniCartConfig($Config) {
		return Config::inst()->get('MiniCart', $Config);
	}

}

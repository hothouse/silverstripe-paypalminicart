<?php

class MiniCartControllerExtension extends DataExtension {
	public function onAfterInit() {
		Requirements::combine_files(
			'minicart.js',
			array(
				MODULE_PAYPALMINICART_PATH.'/thirdparty/minicart/dist/minicart.min.js',
				MODULE_PAYPALMINICART_PATH.'/js/main.js'
			)
		);
		Requirements::customCss('.MiniCartButton{display:none;}');
	}

	public function PaypalMinicartSuccessPage() {
		if($PaypalMinicartSuccessPage = DataObject::get_one('PaypalMinicartSuccessPage')) {
			return $PaypalMinicartSuccessPage->AbsoluteLink();
		}
	}
}
<?php

class ProductVariation extends DataObject {

	private static $db = array(
		'Title' => 'Varchar(255)',
		'Price' => 'Currency',
		'SortID' => 'Int'
	);

	private static $has_one = array(
		'Page' => 'Page'
	);

	private static $summary_fields = array(
		'Title' => 'Title',
		'Price' => 'Price.Nice'
	);

	private static $default_sort = 'SortID';

	public function getCMSFields() {
		// get fields
		$fields = parent::getCMSFields();

		// remove
		$fields->removeByName('Images');
		$fields->removeByName('SortID');
		$fields->removeByName('PageID');

		return $fields;
	}

}
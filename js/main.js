(function($) {
	$(document).ready(function() {
		if(typeof paypal != 'undefined') {

			// config
			var
				MiniCartButtonSelector = '.MiniCartButton',
				MiniCartButtonTextSelector = '.MiniCartButtonText',
				MiniCartVariationSelector = '.MiniCartVariationSelector',
				PAYPALMINICARTCONFIGURATION = PAYPALMINICARTCONFIGURATION || {
					parent: document.body,
					strings: {
						button: 'Checkout',
						subtotal: 'Subtotal: ',
						discount: 'Discount: ',
						shipping: 'does not include shipping',
						processing: 'Processing...',
						cartoneitem: '[quantity] item ([total])',
						cartmoreitems: '[quantity] items ([total])'
					}
				};

			// price update
			$(MiniCartVariationSelector).change(function() {
				$(this).closest('form').each(function() {
					$(this).find('.product-price').html($(this).find('option:selected').attr('data-price'));
				});
			});

			// render
			paypal.minicart.render(PAYPALMINICARTCONFIGURATION);

			// button
			$('.MiniCartButton').on('click', function() {
				paypal.minicart.view.toggle();
				return false;
			});

			// cart function
			var updateMiniCartButton = function() {
				var quantity = 0;
				var items = paypal.minicart.cart.items();
				for(i = 0; i < items.length; i++) {
					quantity += items[i].get('quantity');
				}

				$(MiniCartVariationSelector).parent().removeClass('has-error');
				$(MiniCartButtonTextSelector).text(
					(quantity == 1 ? PAYPALMINICARTCONFIGURATION.strings.cartoneitem : PAYPALMINICARTCONFIGURATION.strings.cartmoreitems)
					.replace('[quantity]', quantity)
					.replace('[total]', paypal.minicart.cart.total({currencyCode: true, format: true}))
				);
				if(quantity) {
					$(MiniCartButtonSelector).show();
				} else {
					$(MiniCartButtonSelector).hide();
				}
			}

			// events
			paypal.minicart.cart.on('add', function (idx, product, isExisting) {
				if (!product.get('os0')) {
					this.remove(idx);
					$(MiniCartVariationSelector).parent().addClass('has-error');
					alert('Please select an option.');
					return;
				}
				updateMiniCartButton();
			});
			paypal.minicart.cart.on('remove', updateMiniCartButton);
			paypal.minicart.cart.on('destroy', updateMiniCartButton);
			updateMiniCartButton();
		}
	});
})(jQuery);
